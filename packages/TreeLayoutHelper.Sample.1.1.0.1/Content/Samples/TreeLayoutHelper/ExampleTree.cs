﻿/*
------------------------------------------------------------------------------
This source file is a part of Tree Layout Helper.

Copyright (c) 2011 - 2012 Florian Haag

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
------------------------------------------------------------------------------
 */
using System;

using TreeLayoutHelper.TreeLayout;
using TreeLayoutHelper.FlowLayout;
using TreeLayoutHelper.HierarchyLayout;

namespace TreeLayoutHelper.Samples.Example
{
	/// <summary>
	/// Stores an example for a tree along with helper objects.
	/// </summary>
	internal class ExampleTree
	{
		public ExampleTree(string title, TreeNode root, INodeTransformer<TreeNode> treeNodeTransformer, IFlowNodeTransformer<TreeNode> flowNodeTransformer, INodeTransformer<TreeNode> hierarchyNodeTransformer)
		{
			this.title = title;
			this.root = root;
			
			var navigator = new TreeNodeNavigator();
			
			{
				var treeTransformer = new TreeLayoutTransformer<TreeNode>(navigator, treeNodeTransformer);
				this.treeModel = treeTransformer.TransformTree(root);
			}
			
			{
				var flowTransformer = new FlowLayoutTransformer<TreeNode>(navigator, flowNodeTransformer);
				this.flowModel = flowTransformer.TransformTree(root);
			}
			
			{
				var hierarchyTransformer = new HierarchyLayoutTransformer<TreeNode>(navigator, hierarchyNodeTransformer);
				this.hierarchyModel = hierarchyTransformer.TransformTree(root);
				this.hierarchyModel.LevelIndentation = 20;
			}
		}
		
		private readonly string title;
		
		public override string ToString()
		{
			return title;
		}
		
		private readonly TreeNode root;
		
		public TreeNode Root {
			get {
				return root;
			}
		}
		
		public void SetOrientation(Orientation orientation)
		{
			treeModel.Orientation = orientation;
			flowModel.Orientation = orientation;
			hierarchyModel.Orientation = orientation;
		}
		
		private readonly TreeLayoutModel<TreeNode> treeModel;
		
		public TreeLayoutModel<TreeNode> TreeModel {
			get {
				return treeModel;
			}
		}
		
		private readonly FlowLayoutModel<TreeNode> flowModel;
		
		public FlowLayoutModel<TreeNode> FlowModel {
			get {
				return flowModel;
			}
		}
		
		private readonly HierarchyLayoutModel<TreeNode> hierarchyModel;
		
		public HierarchyLayoutModel<TreeNode> HierarchyModel {
			get {
				return hierarchyModel;
			}
		}
		
		#region GDI+ drawing
		public void DrawTree(System.Drawing.Graphics g)
		{
			var canvas = new MultiPurposeCanvas(g);
			
			this.TreeModel.Canvas = canvas;
			canvas.InitializeDrawing(this.TreeModel);
			this.TreeModel.Draw();
			canvas.FinalizeDrawing(this.TreeModel);
		}
		
		public void DrawFlow(System.Drawing.Graphics g)
		{
			var canvas = new MultiPurposeCanvas(g);
			
			this.FlowModel.Canvas = canvas;
			canvas.InitializeDrawing(this.FlowModel);
			this.FlowModel.Draw();
			canvas.FinalizeDrawing(this.FlowModel);
		}
		
		public void DrawHierarchy(System.Drawing.Graphics g)
		{
			var canvas = new MultiPurposeCanvas(g);
			
			this.HierarchyModel.Canvas = canvas;
			canvas.InitializeDrawing(this.HierarchyModel);
			this.HierarchyModel.Draw();
			canvas.FinalizeDrawing(this.HierarchyModel);
		}
		#endregion
	}
}
