﻿/*
------------------------------------------------------------------------------
This source file is a part of Tree Layout Helper.

Copyright (c) 2012 Florian Haag

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
------------------------------------------------------------------------------
 */
using System;

using TreeLayoutHelper.FlowLayout;

namespace TreeLayoutHelper.Samples.Example
{
	internal static class ExampleTreeFactory
	{
		private class BooleanFlowTransformer : IFlowNodeTransformer<TreeNode>
		{
			public ElementType ClassifyNode(TreeNode node)
			{
				switch (node.Text) {
					case "and":
					case "not":
						return ElementType.Sequence;
					case "or":
					case "xor":
						return ElementType.Parallel;
					default:
						return ElementType.Node;
				}
			}
			
			public void AddJunctionNodes(TreeNode parallelNode, ref bool junctionBefore, ref bool junctionAfter)
			{
				switch (parallelNode.Text) {
					case "or":
						junctionAfter = (parallelNode.Parent != null) && (parallelNode.Parent.Text == "xor");
						break;
					case "xor":
						junctionAfter = true;
						break;
				}
			}
			
			public bool HasNodeFrame(TreeNode node)
			{
				return node.Text == "not";
			}
		}
		
		public static ExampleTree CreateBooleanTree()
		{
			var root = new TreeNode("and",
			                        new TreeNode("and",
			                                     new TreeNode("or",
			                                                  new TreeNode("a"),
			                                                  new TreeNode("b")),
			                                     new TreeNode("and",
			                                                  new TreeNode("c"),
			                                                  new TreeNode("or",
			                                                               new TreeNode("d"),
			                                                               new TreeNode("e")))),
			                        new TreeNode("or",
			                                     new TreeNode("not",
			                                                  new TreeNode("and",
			                                                               new TreeNode("not",
			                                                                            new TreeNode("f")),
			                                                               new TreeNode("g"))),
			                                     new TreeNode("and",
			                                                  new TreeNode("and",
			                                                               new TreeNode("h"),
			                                                               new TreeNode("or",
			                                                                            new TreeNode("i"),
			                                                                            new TreeNode("and",
			                                                                                         new TreeNode("j"),
			                                                                                         new TreeNode("and",
			                                                                                                      new TreeNode("k"),
			                                                                                                      new TreeNode("l"))))),
			                                                  new TreeNode("m"))));
			
			return new ExampleTree("Boolean Expression",
			                       root,
			                       new BooleanFlowTransformer(),
			                       new BooleanFlowTransformer(),
			                       new BooleanFlowTransformer());
		}
		
		public static ExampleTree CreateBooleanNestedNotTree()
		{
			var root = new TreeNode("not",
			                        new TreeNode("not",
			                                     new TreeNode("not",
			                                                  new TreeNode("not",
			                                                               new TreeNode("not",
			                                                                            new TreeNode("not",
			                                                                                         new TreeNode("not",
			                                                                                                      new TreeNode("not",
			                                                                                                                   new TreeNode("a")))))))));
			
			return new ExampleTree("Boolean Expression with Nested Not-Nodes",
			                       root,
			                       new BooleanFlowTransformer(),
			                       new BooleanFlowTransformer(),
			                       new BooleanFlowTransformer());
		}
		
		public static ExampleTree CreateBooleanNestedOrTree()
		{
			var root = new TreeNode("or",
			                        new TreeNode("or",
			                                     new TreeNode("a"),
			                                     new TreeNode("b")),
			                        new TreeNode("xor",
			                                     new TreeNode("or",
			                                                  new TreeNode("c"),
			                                                  new TreeNode("d")),
			                                     new TreeNode("e")));
			
			return new ExampleTree("Boolean Expression with Nested Or-Nodes",
			                       root,
			                       new BooleanFlowTransformer(),
			                       new BooleanFlowTransformer(),
			                       new BooleanFlowTransformer());
		}
	}
}
