﻿/*
------------------------------------------------------------------------------
This source file is a part of Tree Layout Helper.

Copyright (c) 2011 Florian Haag

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
------------------------------------------------------------------------------
 */
using System;
using System.Linq;

namespace TreeLayoutHelper.Samples.Example
{
	/// <summary>
	/// Represents a single tree node.
	/// </summary>
	internal class TreeNode
	{
		/// <summary>
		/// Initializes a new instance.
		/// </summary>
		/// <param name="text">The title of the node.</param>
		/// <param name="children">The child nodes.</param>
		/// <exception cref="ArgumentNullException">Any of the arguments or any of the elements in <paramref name="children"/> is <see langword="null"/>.</exception>
		public TreeNode(string text, params TreeNode[] children)
		{
			if (text == null) {
				throw new ArgumentNullException("text");
			}
			if (children == null) {
				throw new ArgumentNullException("children");
			}
			if (children.Contains(null)) {
				throw new ArgumentNullException("One of the elements in children was null.", (Exception)null);
			}
			
			this.text = text;
			this.children = (TreeNode[])children.Clone();
			foreach (var child in children) {
				child.parent = this;
			}
		}
		
		/// <summary>
		/// The child nodes of the node.
		/// </summary>
		private readonly TreeNode[] children;
		
		/// <summary>
		/// Returns the number of child nodes.
		/// </summary>
		public int ChildCount {
			get {
				return children.Length;
			}
		}
		
		/// <summary>
		/// Returns a child node at a given position in the list of child nodes.
		/// </summary>
		/// <param name="index">The position of the child node.</param>
		/// <returns>The child node.</returns>
		/// <exception cref="ArgumentOutOfRangeException"><paramref name="index"/> is less than zero or greater than or equal to <see cref="ChildCount"/>.</exception>
		public TreeNode GetChild(int index)
		{
			return children[index];
		}
		
		/// <summary>
		/// The title of the node.
		/// </summary>
		/// <seealso cref="Text"/>
		private readonly string text;
		
		/// <summary>
		/// The title of the node.
		/// </summary>
		public string Text {
			get {
				return text;
			}
		}
		
		/// <summary>
		/// The parent node.
		/// </summary>
		private TreeNode parent;
		
		/// <summary>
		/// The parent node.
		/// </summary>
		/// <seealso cref="Parent"/>
		public TreeNode Parent {
			get {
				return parent;
			}
		}
		
		/// <summary>
		/// The string being checked by the expression.
		/// </summary>
		/// <seealso cref="CheckedString"/>
		private string checkedString;
		
		/// <summary>
		/// The string being checked by the expression.
		/// </summary>
		public string CheckedString {
			get {
				return checkedString;
			}
			set {
				if (value != checkedString) {
					checkedString = value;
					foreach (var child in children) {
						child.CheckedString = value;
					}
					
					if (parent == null) {
						UpdateFlowResult(true);
					}
				}
			}
		}
		
		/// <summary>
		/// Returns the result of the evaluation, if available.
		/// </summary>
		public bool? EvaluationResult {
			get {
				if (string.IsNullOrEmpty(checkedString)) {
					return null;
				} else {
					switch (this.text) {
						case "and":
							return children.All(child => child.EvaluationResult == true);
						case "or":
							return children.Any(child => child.EvaluationResult == true);
						case "xor":
							return children.Count(child => child.EvaluationResult == true) == 1;
						case "not":
							return !children[0].EvaluationResult.Value;
						default:
							return checkedString.Contains(this.text);
					}
				}
			}
		}
		
		private bool? flowResult;
		
		public bool? FlowResult {
			get {
				return flowResult;
			}
		}
		
		public void UpdateFlowResult(bool? incomingResult)
		{
			if (string.IsNullOrEmpty(checkedString)) {
				flowResult = null;
				foreach (var child in children) {
					child.UpdateFlowResult(null);
				}
			} else {
				switch (this.text) {
					case "and":
						flowResult = incomingResult;
						foreach (var child in children) {
							child.UpdateFlowResult(flowResult);
							flowResult = child.FlowResult;
						}
						break;
					case "or":
						foreach (var child in children) {
							child.UpdateFlowResult(incomingResult);
						}
						flowResult = incomingResult.Value && children.Any(child => child.FlowResult == true);
						break;
					case "xor":
						foreach (var child in children) {
							child.UpdateFlowResult(incomingResult);
						}
						flowResult = incomingResult.Value && (children.Count(child => child.FlowResult == true) == 1);
						break;
					case "not":
						children[0].UpdateFlowResult(incomingResult);
						flowResult = incomingResult.Value && !children[0].FlowResult.Value;
						break;
					default:
						flowResult = incomingResult.Value && this.EvaluationResult.Value;
						break;
				}
			}
		}
		
		public override string ToString()
		{
			return text ?? "";
		}
	}
}
