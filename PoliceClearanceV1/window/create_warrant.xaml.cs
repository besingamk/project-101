﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Data.EntityClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PoliceClearanceV1.window
{
    /// <summary>
    /// Interaction logic for create_warrant.xaml
    /// </summary>
    public partial class create_warrant : ModernDialog
    {
        private int newAddedWarrantId = 0;
        police_entity ef = new police_entity();
        public int warrant_id;
        public create_warrant()
        {
            InitializeComponent();

            add_suspect.IsEnabled = false;
            // define the dialog buttons
            this.Buttons = new Button[] { };
        }

        private void create_new_warrant_cancel(object sender, RoutedEventArgs e)
        {
            PoliceClearanceV1.window.create_warrant cWarrant = new PoliceClearanceV1.window.create_warrant();
            this.Hide();
            //cWarrant.Hide();
        }

        private void new_suspect_init(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(this.warrant_id.ToString());
            new_suspect new_suspect = new new_suspect(this.warrant_id);
            new_suspect.Show();
        }

        private void new_warrant_save_btn(object sender, RoutedEventArgs e)
        {
            string cNumber = case_number.Text;
            string cRime = crime.Text;
            string cDescription = case_desc.Text;
            string cRemarks = case_remarks.Text;
            decimal bAmmount = bail_ammount.Value;
            string issuedOn = issued_on.Text;
            string issuedBy = issued_by.Text;
            string addressOne = address_one.Text;
            string addressTwo = address_two.Text;
            string iBarangay = barangay.Text;
            string iCity = city.Text;
            string iProvince = province.Text;

            var messages = new List<string>();


            warrant new_warrant = new warrant();
            new_warrant.case_number = cNumber;
            new_warrant.crime = cRime;
            new_warrant.case_description = cDescription;
            new_warrant.case_remarks = cRemarks;
            new_warrant.bail_amount = bAmmount;
            new_warrant.issued_by = issuedBy;
            new_warrant.issued_on = issuedOn;
            new_warrant.address_one = addressOne;
            new_warrant.address_two = addressTwo;
            new_warrant.barangay = iBarangay;
            new_warrant.city = iCity;
            new_warrant.province = iProvince;

            ef.warrants.Add(new_warrant);
            ef.SaveChanges();
            this.newAddedWarrantId = new_warrant.WarrantID;
            

            using (var ctx = new police_entity())
            {
                //var warrant_id = ctx.warrants.SqlQuery("select WaarantID from warrant order by WarrantID desc limit 1").FirstOrDefault<warrant>();
                foreach (var warrant in ctx.warrants)
                {
                    if (warrant.crime == cRime || warrant.case_number == cNumber)
                    {
                        this.warrant_id = warrant.WarrantID;
                    }
                }
            }

            //save_success_new_warrant save_true = new save_success_new_warrant();
            //save_true.Show();

            PoliceClearanceV1.window.alert save_true = new PoliceClearanceV1.window.alert();
            save_true.Title = "Save succesfully";
            messages.Add("You have successfully save a new warrant. Add some suspect.");
            save_true.set_list(messages);
            //save_true.Width = 400;
            save_true.ShowDialog();

            add_suspect.IsEnabled = true;
            
        }

        private void CurrencyEditor_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }
    }
}
