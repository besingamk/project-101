﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PoliceClearanceV1.window
{
    /// <summary>
    /// Interaction logic for new_suspect.xaml
    /// </summary>
    public partial class new_suspect : ModernDialog
    {
        public int warrant_id;

        police_entity ef = new police_entity();
        public new_suspect(int warrant_id)
        {
            InitializeComponent();
            arrestStatus.Items.Add("mark Kevin beinga");
            //create_warrant war = new create_warrant();
            //this.warrant_id = warrant_id;
            //MessageBox.Show(warrant_id.ToString());

            // define the dialog buttons
            this.Buttons = new Button[] {};
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            //string firstname = suspect_firstname.Text;
            //string lastname = suspect_lastname.Text;
            //string contact = suspect_contact.Text;
            //string address = suspect_address.Text;

            //suspect sus = new suspect();
            //sus.WarrantID = this.warrant_id;
            //sus.firstname = firstname;
            //sus.lastname = lastname;
            //sus.contact = contact;
            //sus.address = address;
            //ef.suspects.Add(sus);
            //ef.SaveChanges();

            //success_adding_suspect suspect_added = new success_adding_suspect();
            //suspect_added.Show();
        }

        private void GridSplitter_DragDelta_1(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {

        }
    }
}
