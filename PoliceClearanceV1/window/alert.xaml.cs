﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PoliceClearanceV1.window
{
    /// <summary>
    /// Interaction logic for alert.xaml
    /// </summary>
    public partial class alert : ModernDialog
    {
        public List<string> alert_messages;
        public alert()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            this.Title = "Alert Box";            
            this.Buttons = new Button[] { this.OkButton };
        }

        public void set_list(List<string> list)
        {
            alert_messages = list;
            this.alert_messages.ToArray();

            List<Errors> error = new List<Errors>();

            
            foreach (var alerts in alert_messages)
            {
                error.Add(new Errors() { message = alerts });
            }

            errors.ItemsSource = error;
        }
    }

    public class Errors
    {
        public string message { get; set; }
    }
}
