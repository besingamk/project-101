﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PoliceClearanceV1.window
{
    /// <summary>
    /// Interaction logic for login_dialog.xaml
    /// </summary>
    public partial class login_dialog : ModernDialog
    {
        police_entity PoliceEntity = new police_entity();
        public login_dialog()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

           

            // define the dialog buttons
            //this.Buttons = new Button[] { this.OkButton, this.CancelButton };
            this.Buttons = new Button[] { };
        }

        //private void close(object sender, EventArgs e)
        //{
        //   MessageBox.Show("closing");
        //}

        private void close(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("closing");
            this.Close();
        }

        private void auth(object sender, RoutedEventArgs e)
        {
            PoliceClearanceV1.window.alert alert = new PoliceClearanceV1.window.alert();
            string username = login_username.Text;
            string password = login_password.Password;

            var messages = new List<string>();

            if (!String.IsNullOrEmpty(username))
            {
                using (police_entity login_credential = new police_entity())
                {
                    IQueryable<User> usernameQuery = from User in login_credential.Users
                                                     where User.username == username
                                                     where User.password == password
                                                     select User;
                    var a = usernameQuery.ToArray();
                    if (a.Length != 1)
                    {
                        alert.Title = "Login Error";
                        messages.Add("wrong username or password");
                        alert.set_list(messages);
                        alert.ShowDialog();
                    }
                    else
                    {
                        Window main = new PoliceClearanceV1.MainWindow();
                        this.Hide();
                        main.Show();
                    }
                    //MessageBox.Show(a.Length.ToString());

                    foreach (var asadf in usernameQuery)
                    {
                        //asadf.
                    }
                }
            }
            else
            {
                alert.Title = "Login Error";
                messages.Add("username and password is require.");
                alert.set_list(messages);
                alert.ShowDialog();
            }
            

            //foreach (var user in PoliceEntity.Users)
            //{
            //    if (username == user.username && password == user.password)
            //    {
            //        Window main = new PoliceClearanceV1.MainWindow();
            //        Log logs = new Log();
            //        logs.UsersID = user.UserID;
            //        logs.logs = "system initialize";
            //        PoliceEntity.Logs.Add(logs);
            //        PoliceEntity.SaveChanges();

            //        this.Hide();
            //        main.Show();
            //    }
            //    else
            //    {
            //        PoliceClearanceV1.window.alert alert = new PoliceClearanceV1.window.alert();
            //        alert.Title = "Login Error";

            //        if (String.IsNullOrEmpty(username))
            //        {
            //            messages.Add("username is require");
            //        }

            //        if (String.IsNullOrEmpty(password))
            //        {
            //            messages.Add("password is require");
            //        }

            //        alert.set_list(messages);
                    
            //        alert.ShowDialog();
            //        //MessageBox.Show("cant");
            //    }
            //}

            //var credentials = new List<KeyValuePair<string, string>>();
            //credentials.Add(new KeyValuePair<string, string>("mark", "password"));
            //credentials.Add(new KeyValuePair<string, string>("kevin", "password"));
            //credentials.Add(new KeyValuePair<string, string>("admin", "superpassword"));
            //credentials.Add(new KeyValuePair<string, string>("user1", "password123"));
            //credentials.Add(new KeyValuePair<string, string>("besingamk", "0401"));

            //if (credentials.Contains(new KeyValuePair<string, string>(username, password)))
            //{
            //    Window main = new PoliceClearanceV1.MainWindow();
            //    this.Hide();
            //    main.Show();

            //}
            //else
            //{
            //    MessageBox.Show("NO HAS");
            //}
        }
    }
}
