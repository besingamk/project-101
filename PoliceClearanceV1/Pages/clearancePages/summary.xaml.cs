﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PoliceClearanceV1.Pages.clearancePages
{
    /// <summary>
    /// Interaction logic for summary.xaml
    /// </summary>
    public partial class summary : UserControl
    {
        public summary()
        {
            InitializeComponent();
        }

        private void saveApplication(object sender, RoutedEventArgs e)
        {
            PoliceClearanceV1.Pages.clearancePages.personalinfo Pi = new personalinfo();
            string firstName = Pi.firstname.Text;
            MessageBox.Show(firstName);
        }

        
    }
}
