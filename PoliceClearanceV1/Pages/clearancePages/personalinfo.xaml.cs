﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PoliceClearanceV1.Pages.clearancePages
{
    /// <summary>
    /// Interaction logic for personalinfo.xaml
    /// </summary>

    class Gender
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public partial class personalinfo : UserControl
    {
        public personalinfo()
        {
            InitializeComponent();

            var gender = new List<Gender>();
            gender.Add(new Gender { name = "male", value = "male" } );
            gender.Add(new Gender { name = "female", value = "female" });

            //this.pi_gender.DataContext = gender;

            

            //test.Visibility = Visibility.Hidden;
        }
    }
}
