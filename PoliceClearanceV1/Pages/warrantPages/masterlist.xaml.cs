﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PoliceClearanceV1.Pages.warrantPages
{
    /// <summary>
    /// Interaction logic for masterlist.xaml
    /// </summary>
    public partial class masterlist : UserControl
    {
        police_entity policeClearance = new police_entity();
        public masterlist()
        {
            InitializeComponent();
            firstnamePlaceholder = "First Name";
            middlenamePlaceholder = "Middle Name";
            lastnamePlaceholder = "Last Name";
            this.DataContext = this;

            warrantList.ItemsSource = policeClearance.warrants.ToArray();
            
        }

       public string firstnamePlaceholder { get; set;}
       public string middlenamePlaceholder { get; set; }
       public string lastnamePlaceholder { get; set; }

       private void removePlaceholder(object sender, RoutedEventArgs e)
       {

       }

       private void create_new_warrant(object sender, RoutedEventArgs e)
       {
           PoliceClearanceV1.window.create_warrant create_warrant_window = new PoliceClearanceV1.window.create_warrant();
           create_warrant_window.Show();
       }
    }
}
